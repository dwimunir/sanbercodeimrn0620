//Soal Nomor 1

class Animal {
    constructor(name){
        this._name = name 
        this._legs = 4 
        this._cold_blooded = false 
    }

    get name(){
        return this._name
    }
    set name(x){
        this._name = x
    }

    get legs(){
        return this._legs
    } 
    set legs(x){
        this._legs = x
    }

    get cold_blooded(){
        return this._cold_blooded
    }
    set cold_blooded(x){
        return this._cold_blooded = x
    }
}
 
var sheep = new Animal("shaun"); //nah name yang di constructor itu nilainya dari
//"shaun" itu yang nanti dikirim dan di terima dengan nama name di atas
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// Code class Ape dan class Frog 

class Ape extends Animal{
    constructor (name){
        super(name)
        this._yell = "Auooo"
        this.legs = 2
    }
    yell(){
        console.log(this._yell)
    }
}
 
class Frog extends Animal{
    constructor(name){
        super(name)
        this._jump = "hop hop"
    }
    jump(){
        console.log(this._jump)
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

//soal nomor 2

class Clock{
    constructor(template){
        this._template = template
        this._timer
    }

    render(){
      var date = new Date()

      var hours = date.getHours()
      if (hours < 10) hours = '0' + hours;
      
      var mins = date.getMinutes()
      if (mins < 10) mins = '0' + mins;
      
      var secs = date.getSeconds()
      if (secs < 10) secs = '0' + secs;
  
      var output = this._template.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    stop() {
      clearInterval(this._timer);
    };
  
    start() {
      this.render();
      this._timer = setInterval(() => this.render(),1000);
    };
  
  }
  
  var clock = new Clock({template: 'h:m:s'});
  clock.start(); 