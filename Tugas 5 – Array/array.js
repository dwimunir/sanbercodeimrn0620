//Soal Nomor 1

function range(startNum, finishNum){
  var arr = []
  if(startNum && finishNum !== undefined && startNum <finishNum){
    for(startNum; startNum<=finishNum; startNum++){
      arr.push(startNum)
    }
    return arr
  }else if(startNum>finishNum){
    for(startNum; startNum>=finishNum; startNum--){
      arr.push(startNum)
    }
    return arr
  }
  else return -1
}

console.log(range(9,1))
console.log('\n')

//Soal Nomor 2

function rangeWithStep(startNum, finishNum, step) {
  var arr = []
  if(startNum < finishNum) {
    for(startNum; startNum < finishNum; startNum+=step){
      arr.push(startNum)
    }
    return(arr)
  }
  else {
    for(startNum; startNum > finishNum; startNum-=step){
      arr.push(startNum)
    }
    return(arr)
  }
}

console.log(rangeWithStep(20,10,3))
console.log('\n')

// Soal Nomor 3

function sum(startNum, finishNum, step = 1) {
  var total=0
  if(startNum < finishNum) {
    for(startNum; startNum <= finishNum; startNum+=step){
      total += startNum
    }
    return total
  }
  else {
    for(startNum; startNum >= finishNum; startNum-=step){
      total += startNum
    }
    return total
  }
}
console.log(sum(20,10,2))

// Soal Nomor 4

function dataHandling(input){
  for(let a=0; a<input.length; a++){
    console.log(`Nomor ID: ${input[a][0]}`)
    console.log(`Nama Lengkap: ${input[a][1]}`)
    console.log(`TTL: ${input[a][2]} ${input[a][3]} `)
    console.log(`Hobi: ${input[a][4]}`)
    console.log('\n')
  }
}

var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

dataHandling(input)

//Soal Nomor 5

function balikKata(str){
  let arr= ""
  for(let i = str.length; i>= 0; i-- ){
    arr+= str.charAt(i)
  }
  console.log(arr)
}

balikKata("Kasur Rusak")

//Soal Nomor 6

function dataHandling2(input){
  input.splice(2,1,"Provinsi Bandar Lampung")
  input.splice(4,1,"Pria", "SMA Internasional Metro")
  console.log(input)

  var bulan = input[3]
  var bulanH=bulan.split('/')
  switch(bulanH[1]){
    case '01':console.log('Januari'); break;
    case '02':console.log('Februari'); break;
    case '03':console.log('Maret'); break;
    case '04':console.log('April'); break;
    case '05':console.log("Mei"); break;
    case '06':console.log("Juni"); break;
    case '07':console.log("Juli"); break;
    case '08':console.log("Agustus"); break;
    case '09':console.log("September"); break;
    case '10':console.log("Oktober"); break;
    case '11':console.log("November"); break;
    case '12':console.log("Desember"); break;
  }
  console.log(bulanH.sort(function(a, b){return b - a}))
  console.log(bulan.split("/").join("-"))
  console.log(input[1].slice(0,15))
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);