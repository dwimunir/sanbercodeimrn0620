//Soal Nomor 1

var nama  = "Kambing Jantan"
var peran = "Guard"

var peranLowerCase = peran.toLowerCase();

if(nama !== ""){
    if(peran !== ""){
        console.log(`Selamat datang di Dunia Werewolf, ${nama}`)
        if(peranLowerCase === "penyihir"){
            console.log(`Halo Penyihir ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`)
        }
        else if(peranLowerCase === "guard"){
            console.log(`Halo Guard ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf.`)
        }
        else if(peranLowerCase === "werewolf"){
            console.log(`Halo Werewolf ${nama}, Kamu akan memakan mangsa setiap malam!`)
        }
        else console.log("Mari berpetualang di dunia Werewolf")
    }else console.log(`Halo ${nama}, pilih peranmu untuk memulai game!!`)
}else console.log("Nama Harus Di Isi!!!")

//Soal Nomor 2

var hari = 21; 
var bulan = 1; 
var tahun = 1945;

switch(bulan){
    case 1 : {bulan = "Januari";break;}
    case 2 : {bulan = "Februari";break;}
    case 3 : {bulan = "Maret";break;}
    case 4 : {bulan = "April";break;}
    case 5 : {bulan = "Mei";break;}
    case 6 : {bulan = "Juni";break;}
    case 7 : {bulan = "Juli";break;}
    case 8 : {bulan = "Agustus";break;}
    case 9 : {bulan = "September";break;}
    case 10 : {bulan = "Oktober";break;}
    case 11 : {bulan = "November";break;}
    case 12 : {bulan = "Desember";break;}
}

console.log(hari+" "+bulan+" "+tahun)