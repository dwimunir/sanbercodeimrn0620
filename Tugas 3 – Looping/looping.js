//Soal Nomor 1

console.log("Looping Pertama \n")

var a1 = 2

while(a1 <= 20 ){
    console.log(`${a1} - I love coding`)

    a1 += 2
}
console.log('\n')
console.log("Looping Kedua \n")

var a1a = 20

while(a1a >= 2 ){
    console.log(`${a1a} - I will become a mobile developer`)

    a1a -= 2
}

console.log('\n')
//Soal Nomor 2

for(var a2= 1; a2<=20; a2++){
    if(a2 % 2 == 0){
        console.log(`${a2} - Berkualitas`)
    }
    else if(a2 % 3 == 0){
        console.log(`${a2} - I Love Coding`)
    }
    else{
        console.log(`${a2} - Santai`)
    }
}

console.log('\n')
//Soal Nomor 3

for(var lebar=1; lebar<=4; lebar++){
    for(var panjang=1; panjang<=8;panjang++){
        process.stdout.write('#')
    }
    console.log('')
}

console.log('\n')
//Soal Nomor 4

for(var a4 =1; a4 <= 7; a4++){
    for(var b4= 1; b4<=a4; b4++){
        process.stdout.write('#')
    }
    console.log('')
}

console.log('\n')
//Soal Nomor 5

for(var a5= 1; a5<=8; a5++ ){
    if(a5 % 2 == 0){
        for(b51=1; b51 <= 8; b51++){
            if(b51 % 2 == 0){
                process.stdout.write(' ')
            }
            else process.stdout.write('#')
        }
    }
    else{
        for(b52=1; b52 <= 8; b52++){
            if(b52 % 2 == 0){
                process.stdout.write('#')
            }
            else process.stdout.write(' ')
        }
    }
    console.log('')
}