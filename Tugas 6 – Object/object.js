//Soal Nomor 1

function arrayToObject(arr) {
    // Code di sini 
    for(let i = 0; i<arr.length; i++){
        var ageP = ""
        var date = new Date()
        var year = date.getFullYear()
        if(arr[i][3] === undefined || arr[i][3] > year){
            ageP = "Invalid Birth Year"
        }
        else{
            ageP = year -arr[i][3]
        }
        var object = {
            firstName : arr[i][0],
            lastName : arr[i][1],
            gender : arr[i][2],
            age: ageP
        }
        process.stdout.write(`${i+1}. ${arr[i][0]} ${arr[i][1]}: `)
        console.log(object)
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 

console.log('\n')
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 

console.log('\n')

//Soal Nomor 2

function shoppingTime(memberId, money) {
    // you can only write your code here!
    var barang = [
        {
            nama: "Sepatu brand Stacattu",
            harga: 1500000,
        },
        {
            nama: "Baju brand Zoro",
            harga: 500000,
        },
        {
            nama: "Baju brand H&N",
            harga: 250000,
        },
        {
            nama: "Sweater brand Uniklooh",
            harga: 175000,
        },
        {
            nama: "Casing Handphone",
            harga: 50000,
        }
    ]
    var uang = money
    if(memberId !== '' && memberId !== undefined){
        if(uang >= 50000){
            var listP=[]
            for(let i =0; i<barang.length;i++){
                if(uang >= barang[i].harga){
                    uang -= barang[i].harga
                    listP.push(barang[i].nama)
                }
            }
            var list = {
                memberId: memberId,
                money: money,
                listPurchased:listP ,
                changeMoney: uang
            }
            return list
        }
        else return "Mohon maaf, uang tidak cukup"
    }
    else{
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }
  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
  console.log(shoppingTime('82Ku8Ma742', 170000));
  console.log(shoppingTime('', 2475000)); 
  console.log(shoppingTime('234JdhweRxa53', 15000)); 
  console.log(shoppingTime());

  //Soal Numor 3

  function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    var dataPenumpang = []
    for(let i =0; i < arrPenumpang.length; i++){
        var cost = (rute.indexOf(arrPenumpang[i][2]) - rute.indexOf(arrPenumpang[i][1])) * 2000
        var object = {
            penumpang: arrPenumpang[i][0],
            naikDari: arrPenumpang[i][1],
            tujuan: arrPenumpang[i][2],
            bayar: cost
        }
        dataPenumpang.push(object)
    }
    return dataPenumpang
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]