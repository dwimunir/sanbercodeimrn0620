var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

function baca(index, time){
    if(index > books.length-1){
        return ;
    }
    else{
        readBooksPromise(time,books[index])
        .then(function(times){
            return index + baca(index+1,times)
        })
        .catch(function(error){
            return error
        })
    }
}

baca(0,10000)